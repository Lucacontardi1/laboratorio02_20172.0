package it.unimi.di.sweng.lab02;

import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class BowlingTest {
	
	public static final int MAX_ROLS =20;

	@Rule
    public Timeout globalTimeout = Timeout.seconds(2);

	private Bowling game;

	@Before
	public void setUp(){
		game = new BowlingGame();
	}
	
	public void rollMany(int time,int pins){
		for(int i=0; i<time; i++)
			game.roll(pins);
	}
	
	private void nTiri(int[] argoment, int risultato_atteso){
		for(int i=0; i<argoment.length; i++)
			game.roll(argoment[i]);
		rollMany(MAX_ROLS-argoment.length,0);
		assertThat(game.score()).isEqualTo(risultato_atteso);
	}
	
	
	@Test
	public void gutterGame() {
		rollMany(MAX_ROLS,0);	
		assertThat(game.score()).isEqualTo(0);
	}
	
	@Test
	public void allOnesGame() {
		rollMany(MAX_ROLS,1);		
		assertThat(game.score()).isEqualTo(20);
	}
	
	@Test
	public void oneSpareGame() {
		int[] tiri = {5, 5, 3};
		nTiri(tiri, 16);
	}
	
	@Test
	public void notSpareGame() {
		 // Il test deve simulare una partita in cui vengono colpiti 5 birilli due volte di seguito,
		 // ma non all'interno del medesimo frame.
		 // Questa condizione non deve essere riconosciuta come spare.
		 // Es., roll(1), roll(5), roll(5), ecc., non è spare.		game.roll(5);

		int[] tiri = {1, 5, 5};
		nTiri(tiri, 11);
	}
	
	@Test
	public void oneStrikeGame() {
		 // Il test deve simulare una partita in cui avviene uno strike.
		 // Es., lo score di: roll(10), roll(3), roll(4), roll(0), ..., roll(0), è 24.
		int[] tiri = {10, 3, 4, 0};
		nTiri(tiri, 24);
	}
	
	@Test
	public void notStrikeGame() {
		 // Il test deve simulare una partita in cui vengono colpiti 10 birilli con il secondo tiro di un frame,
		 // Questa condizione deve essere riconosciuta come spare e non come strike.
		 // Es., lo score di: roll(0), roll(10), roll(3), roll(0), ..., roll(0), è 16.
		int[] tiri = {0, 10, 3, 0};
		nTiri(tiri, 16);
		
		
	}
	
/*	@Test
	public void lastFrameStrikeGame() {
		 // Il test deve simulare una partita in cui avviene uno strike nell'ultimo frame.
		 // In questo caso il giocatore completa il frame ed ha diritto ad un tiro aggiuntivo.
		 // Es., lo score di: roll(0), ..., roll(0), roll(10), roll(3), roll(2), è 15.
		fail("Not yet implemented.");
	}
	
/*	@Test
	public void perfectGame() { 
		 // Il test deve simulare una partita perfetta in cui avvengono 12 strike di seguito.
		 // Es., lo score di: roll(10), ..., roll(10), è 300.
		fail("Not yet implemented.");
	}
	
/**/
	
}
